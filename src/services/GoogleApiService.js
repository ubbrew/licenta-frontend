import axios from 'axios'

const googleApiClient = axios.create({
  baseURL: `https://www.googleapis.com/books`,
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
  //,
  //timeout: 10000
})

export default {
  getBooksWithQuery(query) {
   delete  googleApiClient.defaults.headers.common['Authorization'] 
    return googleApiClient.get('/v1/volumes?q=' + query+ "&key=AIzaSyCvSTq5WOBW4Z3PEZQl70zFUUWydVu5_yQ" + "&maxResults=" + 40 )
  },
  getBookWithId(id) {
    delete googleApiClient.defaults.headers.common['Authorization'] 
    return googleApiClient.get('v1/volumes/' + id)
  }
}
