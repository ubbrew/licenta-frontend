import axios from 'axios'

const apiClient = axios.create({
  baseURL: `http://localhost:8080`,
  //withCredentials: false, // This is the default
  headers: {
    //Accept: 'application/json',
    'Content-Type': 'application/json'
  }
  //,
  //timeout: 10000
})

export default {
  getToken() {
    apiClient.defaults.headers.common['Authorization'] = localStorage.getItem(
      'token'
    )
  },
  setJwt() {
    apiClient.defaults.headers.common['Authorization'] = localStorage.getItem(
      'token'
    )
  },
  clearToken() {
    apiClient.defaults.headers.common['Authorization'] = null
    localStorage.removeItem('token')
  },

  //USER related rest APIs
  login(credentials) {
    return apiClient.post('/authenticate', credentials).then(({ data }) => {
      apiClient.defaults.headers.common['Authorization'] = `Bearer ${data.jwt}`
      console.log(data.jwt)
      localStorage.setItem('token', `Bearer ${data.jwt}`)
    })
  },

  //for registration
  postUser(user) {
    /*this.clearToken()
    return apiClient.post('/register', user).then(({ data }) => {
      apiClient.defaults.headers.common['Authorization'] = `Bearer ${data.jwt}`
      console.log(data.jwt)
      localStorage.setItem('token', `Bearer ${data.jwt}`)
    })*/
    return apiClient.post('register', user)
  },

  updateUser(user) {
    this.getToken()
    return apiClient.put('/users/' + user.userId, user)
  },

  //imi returneaza userul cu email-ul dat ca si parametru
  getUserData(username) {
    this.getToken()
    return apiClient.get('/users/' + username)
  },
  addBookToRead(dto){
    this.getToken()
    return apiClient.post('/books/addRead?email='+dto.username, dto.book)
  },
  getReadBooksForUser(username) {
    this.getToken()
    return apiClient.get('/books/read?email='+username)
  },
  addBookToWishList(dto){
    this.getToken()
    return apiClient.post('/books/addToWishList?email='+dto.username, dto.bookDat)
  },
  addBookToReadingList(dto){
    this.getToken()
    console.log(dto)
    return apiClient.post('/books/addToReadingList?email='+dto.username, dto.bookDat)
  },
  getWishListedBooksForUser(username) {
    this.getToken()
    return apiClient.get('/books/wishListed?email='+username)
  },
  getReadingBooksForUser(username) {
    this.getToken()
    return apiClient.get('/books/reading?email='+username)
  },
  getBooksRead(username) {
    this.getToken()
    return apiClient.get('/books/read/total?email='+username)
  },
  getReviewedBooks(username) {
    this.getToken()
    return apiClient.get('/books/reviewed/total?email='+username)
  },
  getWishlistBooks(username) {
    this.getToken()
    return apiClient.get('/books/wishlist/total?email='+username)
  },
  addBookToReadingList(dto){
    this.getToken()
    return apiClient.post('/books/reviews?email='+dto.username+"&review="+dto.review+"&rating="+dto.rating, dto.book)  
  },
  getBookWithId(id) {
    this.getToken()
    return apiClient.get('/books/' + id)  
  },
  getRecommendations(dto) {
    this.getToken()
    return apiClient.get('/books/recommendation?username=' + dto.username + '&bookId=' + dto.bookId)  
  }
}
