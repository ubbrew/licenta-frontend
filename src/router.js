import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import SignUp from './views/SignUp.vue'
import HomePage from './views/HomePage.vue'
import BookList from './views/BookList.vue'
import UserProfile from './views/UserProfile.vue'
import UserLibrary from './views/UserLibrary.vue'
import BookPage from './views/BookPage.vue'
import NProgress from 'nprogress'
import store from '@/store/store'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      component: SignUp
    },
    {
      path: '/user/:username',
      name: 'home-page',
      component: BookList,
      meta: { requiresAuth: true },
      props: true
    },
    {
      path: '/user/profile/:username',
      name: 'user-profile',
      component: UserProfile,
      meta: { requiresAuth: true },
      props: true
    },
    {
      path: '/user/:username/library/',
      name: 'user-library',
      component: UserLibrary,
      meta: { requiresAuth: true },
      props: true
    },
    {
      path: '/book/:id',
      name: 'book-page',
      component: BookPage,
      meta: { requiresAuth: true },
      props: true
    }
  ]
})

router.beforeEach((routeTo, routeFrom, next) => {
  NProgress.start()

  const loggedIn = localStorage.getItem('user')
  console.log('logged in user from router:' + loggedIn)
  if (
    routeTo.matched.some(record => record.meta.requiresAuth) &&
    !loggedIn &&
    routeFrom.name != 'login' 
  ) {
    const notification = {
      type: 'error',
      message: 'Nu sunteti logat!'
    }
    store.dispatch('notification/add', notification, { root: true })
    next('/')
    NProgress.done()
  }

  next()
})

router.afterEach(() => {
  NProgress.done()
})

export default router