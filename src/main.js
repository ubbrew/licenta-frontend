import Vue from 'vue'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import App from './App.vue'
import router from './router'
import store from './store/store'
import BaseIcon from '@/components/BaseIcon'
import Vuelidate from 'vuelidate'
import VFC from 'vfc'
import 'vfc/dist/vfc.css'
import Notifications from 'vue-notification'
import Vuetify from 'vuetify'
import {HeartRating} from 'vue-rate-it'

Vue.use(Vuetify)
Vue.use(VFC)
Vue.use(Vuelidate)
Vue.use(Notifications)
Vue.component('BaseIcon', BaseIcon)
Vue.component('heart-rating', HeartRating);
Vue.config.productionTip = false

const requireComponent = require.context(
  './components',
  false,
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, '$1'))
  )

  Vue.component(componentName, componentConfig.default || componentConfig)
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
