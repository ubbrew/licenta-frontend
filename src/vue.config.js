module.exports = {
    devServer: {
        open: process.platform === 'darwin',
        host: 'localhost',
        port: 8081, // CHANGE YOUR PORT HERE!
        https: true,
        hotOnly: false,
    },
}