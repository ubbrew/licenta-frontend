import Vue from 'vue'
import Vuex from 'vuex'
import * as user from '@/store/modules/user.js'
import * as notification from '@/store/modules/notification.js'
import * as book from '@/store/modules/book.js'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    book,
    notification
  },
  state: {}
})
