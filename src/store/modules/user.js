import Service from '@/services/Service.js'

export const namespaced = true

export const state = {
  user: {},
  username: '',
  totalRead: 0,
  totalReviewed: 0,
  totalWishlist: 0
}

export const mutations = {
  FETCH_USERNAME(state, username) {
    state.username = username
    localStorage.setItem('username', JSON.stringify(username))
  },
  FETCH_USER(state, user) {
    state.user = user
    localStorage.setItem('user', JSON.stringify(user))
    console.log('user from local storage: ' + localStorage.getItem('user'))
  },
  SET_READ_TOTAL(state, totalRead) {
    state.totalRead = totalRead
  },
  SET_REVIEWED_TOTAL(state, totalReviewed) {
    state.totalReviewed = totalReviewed
  },
  SET_WISHLIST_TOTAL(state, totalWishlist) {
    state.totalWishlist = totalWishlist
  },
  CLEAR_USER_DATA(state) {
    state.user = {}
    localStorage.removeItem('user')
    localStorage.removeItem('username')
  },
  SET_USER_DATA(state, userData) {
    state.user = userData
    localStorage.setItem('user', JSON.stringify(userData))
    //console.log("user from set u data:")
    //console.log(state.user)
  }
}

export const actions = {
  login({ dispatch, commit }, credentials) {
    console.log('in userstore: ')
    console.log(credentials)
    return Service.login(credentials)
      .then(() => {
        dispatch('fetchUser', credentials.username)
        commit('FETCH_USERNAME', credentials.username)
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message: 'A aparut o eroare la logare: user sau parola incorecte!'
        }
        dispatch('notification/add', notification, { root: true })
        throw error
      })
  },
  registerUser({ commit, dispatch }, user) {
    return Service.postUser(user)
      .then(() => {
        const notification = {
          type: 'success',
          message: 'User adaugat cu succes!'
        }
        dispatch('notification/add', notification, { root: true })
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message:
            'A aparut o problema la crearea unui nou user: ' + error.message
        }
        dispatch('notification/add', notification, { root: true })
        throw error
      })
  },
  fetchUser({ commit, dispatch }, username) {
    return Service.getUserData(username)
      .then(response => {
        commit('FETCH_USER', response.data)
        //console.log("user from fetchUser:")
        //console.log(state.user)
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message: error.message
        }
        store.dispatch('notification/add', notification, { root: true })
        throw error
      })
  },
  fetchUserStatistics({commit, dispatch}) {
    Service.getBooksRead(localStorage.getItem("username")).then(response => {
      commit('SET_READ_TOTAL', response.data)
    })
    Service.getReviewedBooks(localStorage.getItem("username")).then(response => {
      commit('SET_REVIEWED_TOTAL', response.data)
    })
    Service.getWishlistBooks(localStorage.getItem("username")).then(response => {
      commit('SET_WISHLIST_TOTAL', response.data)
    })
  },
  getUserDetails({commit}) {
   return commit('SET_USER_DATA', JSON.parse(localStorage.getItem("user")))
  },
  setUserData({ commit }, userData) {
    commit('SET_USER_DATA', userData)
  },
  logout({ commit }) {
    Service.clearToken()
    commit('CLEAR_USER_DATA')
  }
}

export const getters = {
  getUserId: state => {
    //console.log('user id: ')
    //console.log(state.user.userId)
    return state.user.userId
  }
}
