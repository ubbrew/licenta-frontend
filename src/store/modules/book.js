import GoogleApiService from '@/services/GoogleApiService.js'
import Service from '@/services/Service.js'
export const namespaced = true

export const state = {
  books: [],
  readBooks: [],
  wishListedBooks: [],
  readingBooks: [],
  recommendations: [],
  book: {},
  notif: {}
}

export const mutations = {
  ADD_BOOK(state, book) {
    state.books.push(book)
  },
  SET_BOOKS(state, books) {
    state.books = books
    console.log(state.books)
  },
  ADD_READ_BOOK(state, book) {
    state.readBooks.push(book)
  },
  ADD_WISHLISTED_BOOK(state, book) {
    state.wishListedBooks.push(book)
  },
  SET_READ_BOOKS(state, books) {
    state.readBooks = books
    console.log(state.readBooks)
  },
  SET_WISHLISTED_BOOKS(state, books) {
    state.wishListedBooks = books
  },
  SET_READING_BOOKS(state, books) {
    state.readingBooks = books
  },
  SET_BOOK_RECOMMENDATIONS(state, books) {
    state.recommendations = books
  },
  SET_BOOK(state, book) {
    state.book = book
    localStorage.setItem('book', JSON.stringify(book))
    console.log(localStorage.getItem('book'))
  },
  DELETE_BOOK(state, id) {
    state.books = state.books.filter(function(value) {
      return value.bookId != id
    })
  }
}

export const actions = {
    fetchBooks({ commit, dispatch }, query) {
    return GoogleApiService.getBooksWithQuery(query)
      .then(response => {
        console.log(response.data)
        commit('SET_BOOKS', response.data.items)
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message:
            'There was an error: ' + error.message
        }
        dispatch('notification/add', notification, { root: true })
      })
  },
  
  fetchReadBooks({ commit, dispatch }) {
    return Service.getReadBooksForUser(localStorage.getItem("username"))
      .then(response => {
        console.log(response.data)
        commit('SET_READ_BOOKS', response.data)
      })
  },
  fetchWishListedBooks({ commit }) {
    return Service.getWishListedBooksForUser(localStorage.getItem("username"))
      .then(response => {
        console.log(response.data)
        commit('SET_WISHLISTED_BOOKS', response.data)
      })
  },
  fetchReadingBooks({ commit }) {
    return Service.getReadingBooksForUser(localStorage.getItem("username"))
      .then(response => {
        console.log(response.data)
        commit('SET_READING_BOOKS', response.data)
      })
  },
  fetchBook({ commit, dispatch }, id) {
    return Service.getBookWithId(id)
      .then(response => {
        console.log(response.data)
        commit('SET_BOOK', response.data)
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message:
            'There was an error: ' + error.message
        }
        dispatch('notification/add', notification, { root: true })
      })
  },
  fetchRecommendations({ commit, dispatch }, id) {
    var dto = {
      username: localStorage.getItem("username"),
      bookId : id
    }
    return Service.getRecommendations(dto)
    .then(response => {
      console.log(response.data)
      commit('SET_BOOK_RECOMMENDATIONS', response.data)
    })
  },
  addBookToRead({commit, dispatch}, book) {
    var dto = {
      username: localStorage.getItem("username"),
      book : book
    }
    console.log(dto)
    return Service.addBookToRead(dto)
  },
  addBookReadingList({commit, dispatch}, book) {
    var dto = {
      username: localStorage.getItem("username"),
      book : book
    }
    console.log(dto)
    return Service.addBookToReadingList(dto)
  },
  addBookWishList({commit, dispatch}, bookData) {
    console.log(bookData)
    var dto2 = {
      username: localStorage.getItem("username"),
      bookDat : bookData
    }
    console.log(dto2)
    return Service.addBookToReadingList(dto2)
  },
  reviewBook({commit, dispatch}, dto) {
    var dto2= {
      username: localStorage.getItem("username"),
      book: dto.book,
      review: dto.review,
      rating: dto.rating
    }
    return Service.addBookToReadingList(dto2)
    .then(response => {
      commit('SET_BOOK', response.data)
    })
  },
  setBook({commit}, book) {
    commit('SET_BOOK', book)
  },
  getBook() {
    return localStorage.getItem('book')
  }
}
